#requires -Version 3.0
function New-AemApiAccessToken
{
    param
    (
        [string]$apiUrl,
        [string]$apiKey,
        [string]$apiSecretKey
    )

    # Suppress output and errors by redirecting to $null
    $null = $Error.Clear()

    # Specify security protocols
    try {
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
    }
    catch {
        $null = $Error.Add($_)
    }

    # Convert password to secure string
    $securePassword = ConvertTo-SecureString -String 'public' -AsPlainText -Force

    # Define parameters for Invoke-WebRequest cmdlet
    $params = @{
        Credential  = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList ('public-client', $securePassword)
        Uri         = '{0}/auth/oauth/token' -f $apiUrl
        Method      = 'POST'
        ContentType = 'application/x-www-form-urlencoded'
        Body        = 'grant_type=password&username={0}&password={1}' -f $apiKey, $apiSecretKey
    }

    # Request access token
    try {
        (Invoke-WebRequest @params | ConvertFrom-Json).access_token | Out-File -FilePath .\powershell\token.txt -Force > $null
    }
    catch {
        $null = $Error.Add($_)
    }
}

# Define parameters
$params = @{
    apiUrl        = 'https://zinfandel-api.centrastage.net'
    apiKey        = ''
    apiSecretKey  = ''
}

# Call New-AemApiAccessToken function using defined parameters 
$null = New-AemApiAccessToken @params 2>$null
