'''
The purpose of this script is to do an API call to Duo and get the license counts and saving it to the customer_data.json file under the customer name under the duo key
'''

from __future__ import absolute_import
from __future__ import print_function
import duo_client
import json
import halo

customer_data_file = '../json/customer_data.json'

def org_duo():
    customer_name = 'org Internal'
    # Configuration and information about objects to create.
    admin_api = duo_client.Admin(
        ikey='',
        skey='',
        host='',
    )

    with halo.Halo(text=f'Fetching Duo user count for {customer_name}...', spinner='dots') as spinner:
        try:
            # Retrieve user info from API
            users = admin_api.get_users()
            users_int = len(users)
            total_count = users_int

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the duo value with user count
                    entry['applications']['duo'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'Duo user count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Duo user count for {customer_name}. Error: {str(e)}')
            return

    return total_count


def org2_duo():
    customer_name = 'org2'
    # Configuration and information about objects to create.
    admin_api = duo_client.Admin(
        ikey='',
        skey='',
        host='',
    )

    with halo.Halo(text=f'Fetching Duo user count for {customer_name}...', spinner='dots') as spinner:
        try:
            # Retrieve user info from API
            users = admin_api.get_users()
            users_int = len(users)
            total_count = users_int

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the duo value with user count
                    entry['applications']['duo'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'Duo user count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Duo user count for {customer_name}. Error: {str(e)}')
            return

    return total_count

def org3_duo():
    customer_name = 'org3'
    # Configuration and information about objects to create.
    admin_api = duo_client.Admin(
        ikey='',
        skey='',
        host='',
    )

    with halo.Halo(text=f'Fetching Duo user count for {customer_name}...', spinner='dots') as spinner:
        try:
            # Retrieve user info from API
            users = admin_api.get_users()
            users_int = len(users)
            total_count = users_int

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the duo value with user count
                    entry['applications']['duo'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'Duo user count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Duo user count for {customer_name}. Error: {str(e)}')
            return

    return total_count


def org4_duo():
    customer_name = 'org4'
    # Configuration and information about objects to create.
    admin_api = duo_client.Admin(
        ikey='',
        skey='',
        host='',
    )

    with halo.Halo(text=f'Fetching Duo user count for {customer_name}...', spinner='dots') as spinner:
        try:
            # Retrieve user info from API
            users = admin_api.get_users()
            users_int = len(users)
            total_count = users_int

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the duo value with user count
                    entry['applications']['duo'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'Duo user count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Duo user count for {customer_name}. Error: {str(e)}')
            return

    return total_count


# print('org count' + str(org_duo()))
# print('org2 count' + str(org2_duo()))
# print('org3 count' + str(org3_duo()))
# print('org4 count' + str(org4_duo()))

org_duo()
org2_duo()
org3_duo()
org4_duo()
