'''
Purpose of this script is to sort customer_data.json by customer name alphabetically
'''

import json
import halo

customer_data_file = '../json/customer_data.json'

with halo.Halo(text='Sorting customer data by customer name...', spinner='dots') as spinner:
    with open(customer_data_file, 'r') as f:
        customer_data = json.load(f)

    customer_data = sorted(customer_data, key=lambda k: k['customer'])

    with open(customer_data_file, 'w') as f:
        json.dump(customer_data, f, indent=4)

    spinner.succeed('Customer data sorted successfully.')
