from sys import implementation
from time import time
from pyparsing import empty
import requests
import jmespath
import json
from ssl import CertificateError
import halo

customer_data_file = '../json/customer_data.json'

def org_proofpoint():
    customer_name = 'org Internal'
    url = 'https://us5.proofpointessentials.com/api/v1/orgs/org.com/licensing/'
    username = ''
    password = ''
    license_count = 'license_count'

    with halo.Halo(text='Fetching Proofpoint license count for org Internal...', spinner='dots') as spinner:
        try:
            # Build session
            proofpoint_session = requests.Session()
            headers = {'Content-Type': 'application/json', 'X-user': username, 'X-password': password}

            # This is the actual API get request for information
            response = requests.get(url, headers=headers)
            total_license_count = jmespath.search(license_count, response.json())
            total_count = int(total_license_count)

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the proofpoint value with license count
                    entry['applications']['proofpoint'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed('Proofpoint license count for org Internal fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Proofpoint license count for {customer_name}. Error: {str(e)}')
            return

    return total_count

def org2_proofpoint():
    customer_name = 'org2s'
    url = 'https://us5.proofpointessentials.com/api/v1/orgs/org2web.org/licensing/'
    username = ''
    password = ''
    license_count = 'license_count'

    with halo.Halo(text='Fetching Proofpoint license count for org2 Health & Wellness...', spinner='dots') as spinner:
        try:
            # Build session
            proofpoint_session = requests.Session()
            headers = {'Content-Type': 'application/json', 'X-user': username, 'X-password': password}

            # This is the actual API get request for information
            response = requests.get(url, headers=headers)
            total_license_count = jmespath.search(license_count, response.json())
            total_count = int(total_license_count)

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the proofpoint value with license count
                    entry['applications']['proofpoint'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed('Proofpoint license count for org2 Health & Wellness fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Proofpoint license count for {customer_name}. Error: {str(e)}')
            return

    return total_count

def org3_proofpoint():
    customer_name = 'org3'
    url = 'https://us5.proofpointessentials.com/api/v1/orgs/org3.org/licensing/'
    username = ''
    password = ''
    license_count = 'license_count'

    with halo.Halo(text='Fetching Proofpoint license count for Health Efficient...', spinner='dots') as spinner:
        try:
            # Build session
            proofpoint_session = requests.Session()
            headers = {'Content-Type': 'application/json', 'X-user': username, 'X-password': password}

            # This is the actual API get request for information
            response = requests.get(url, headers=headers)
            total_license_count = jmespath.search(license_count, response.json())
            total_count = int(total_license_count)

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the proofpoint value with license count
                    entry['applications']['proofpoint'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed('Proofpoint license count for Health Efficient fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Proofpoint license count for {customer_name}. Error: {str(e)}')
            return

    return total_count

def org4_proofpoint():
    customer_name = 'org4'
    url = 'https://us5.proofpointessentials.com/api/v1/orgs/org4.co/licensing'
    username = ''
    password = ''

    with halo.Halo(text='Fetching Proofpoint information for org4...', spinner='dots') as spinner:
        try:
            # Build session
            proofpoint_session = requests.Session()
            headers = {'Content-Type': 'application/json', 'X-user': username, 'X-password': password}

            # This is the actual API get request for information
            response = requests.get(url, headers=headers)
            total_count = response.json()

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the proofpoint value with the retrieved information
                    entry['applications']['proofpoint'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed('Proofpoint information for org4 fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Proofpoint information for {customer_name}. Error: {str(e)}')
            return

    return total_count

org_proofpoint()
org2_proofpoint()
org3_proofpoint()
