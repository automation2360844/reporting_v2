#this script reads a JSON file and returns a list of customer names in a function to be used in other scripts

import json

#read JSON file and store in variable
with open('../json/customer_list.json') as f:
    lm_get_groups = json.load(f)
    
#create a function that will iterate through lm_get_groups and return the id and name of each group in a list
def customer_list():
    customer_list = []
    for group in lm_get_groups:
        customer_list.append([group['name']])
    return customer_list

