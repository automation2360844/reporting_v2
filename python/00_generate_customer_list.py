#This script does an API call to LogicMonitor and pulls all customer information and writes it to a JSON file to be used in customer_list.py

import requests
import json
import hashlib
import base64
import time
import hmac
import jmespath

file_path = '../json/customer_list.json'

#Account Info
AccessId =''
AccessKey =''
Company = ''

#Request Info
httpVerb ='GET'
resourcePath = '/device/groups/'
data = ''

#write jmespath query to capture subGroups for an ID of 16
groups = 'data.items[?id==`16`].subGroups[]'

#Construct URL 
url = 'https://'+ Company +'.logicmonitor.com/santaba/rest' + resourcePath

#Get current time in milliseconds
epoch = str(int(time.time() * 1000))

#Concatenate Request details
requestVars = httpVerb + epoch + data + resourcePath

#Construct signature
hmac1 = hmac.new(AccessKey.encode(),msg=requestVars.encode(),digestmod=hashlib.sha256).hexdigest()
signature = base64.b64encode(hmac1.encode())

#Construct headers
auth = 'LMv1 ' + AccessId + ':' + signature.decode() + ':' + epoch
headers = {'Content-Type':'application/json','Authorization':auth}

#Make request for groups
response = requests.get(url, data=data, headers=headers).json()  
lm_get_groups = jmespath.search(groups, response)


#write filtered groups to json file
with open(file_path, 'w') as outfile:
    outfile.write(json.dumps(lm_get_groups, indent=4))
    outfile.close()

