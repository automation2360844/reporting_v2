'''
The purpose of this script is to remove all zeros from customer_data.json and replace it with an empty string and save it in the same place
This is done to make the HTML report look cleaner.
'''

import json
import halo

# JSON file location
json_data = '../json/customer_data.json'

# Initialize spinner
spinner = halo.Halo(text='Processing...', spinner='dots')

try:
    # Start the spinner
    spinner.start()

    # Open JSON file
    with open(json_data) as f:
        data = json.load(f)

    # Parse JSON data and replace the 0 with ''
    for i in data:
        for k, v in i.items():
            if isinstance(v, dict):
                for key, value in v.items():
                    if value == 0:
                        v[key] = ''
    # Write JSON data to file
    with open(json_data, 'w') as f:
        json.dump(data, f, indent=4)

    # Stop the spinner and show success
    spinner.succeed('Zeros removed successfully!')

except Exception as e:
    # Stop the spinner and show failure
    spinner.fail(f'Error: {str(e)}')
