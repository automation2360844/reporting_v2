'''
You run this dude. He takes care of the rest.
'''

import subprocess

def generate_report():
    task_list = [
        '00_generate_customer_list.py',
        '01_create_base_JSON.py',
        '02_logicmonitor.py',
        '02.5_logicmonitor_get_cloud.py',
        '03_o365.py',
        '04_duo.py',
        '05_meraki.py',
        '06_proofpoint.py',
        '07_automox.py',
        '08_cradlepoint.py',
        '09_n_able.py',
        '10_amp.py',
        '11_boxwell_umbrella.py',
        '12_org_umbrella.py',
        '13_datto_rmm.py',
        '14_remove_zeros.py',
        '15_json_to_html.py',
        '16_sendemailreport.py',
        'sort_json.py'
    ]

    for task in task_list:
        print('▔' * 50)
        print('Starting task : ' + task)
        print(' ')
        print('▔' * 50)
        subprocess.call(['python3', task])
        print(' ')

generate_report()
