'''
Purpose of this script is to pull data from N-able and push it to the database
'''

import requests
import re
import json
import halo

customer_data_file = '../json/customer_data.json'

def retrieve_data_from_nable(customer_id):
    url = "https://ncod421.n-able.com/dms2/services2/ServerEI2?wsdl=ServerEI2.wsdl"

    payload = f"""<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ei2="http://ei2.nobj.nable.com/">
                    <soap:Header/>
                    <soap:Body>
                        <ei2:deviceList>
                            <ei2:username></ei2:username>
                            <ei2:password></ei2:password>
                            <ei2:settings>
                                <ei2:first></ei2:first>
                                <ei2:second>?</ei2:second>
                                <ei2:key>customerID</ei2:key>
                                <ei2:value>{customer_id}</ei2:value>
                            </ei2:settings>
                        </ei2:deviceList>
                    </soap:Body>
                </soap:Envelope>"""

    response = requests.post(url, data=payload)
    xml_ugly = response.text

    r1 = re.findall("<value>Professional", xml_ugly)
    total_count = len(r1)

    return total_count


def org1_n_able():
    customer_name = 'org1'
    customer_id = 160

    with halo.Halo(text='Fetching data from N-able...', spinner='dots') as spinner:
        try:
            total_count = retrieve_data_from_nable(customer_id)
            spinner.succeed(f'{customer_name} data fetched successfully from N-able.')
        except Exception as e:
            spinner.fail(f'Failed to fetch data from N-able for {customer_name}. Error: {str(e)}')
            return

    with halo.Halo(text='Updating customer data...', spinner='dots') as spinner:
        # Open the customer_data.json file and read the data
        with open(customer_data_file, 'r') as f:
            customer_data = json.load(f)

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the n_able value with license count
                entry['applications']['n_able'] = total_count
                break

        # Write the updated customer data back to the file
        with open(customer_data_file, 'w') as f:
            json.dump(customer_data, f, indent=4)

        spinner.succeed(f'{customer_name} data updated successfully.')

    return total_count


def org2_n_able():
    customer_name = 'org2'
    customer_id = 890

    with halo.Halo(text='Fetching data from N-able...', spinner='dots') as spinner:
        try:
            total_count = retrieve_data_from_nable(customer_id)
            spinner.succeed(f'{customer_name} data fetched successfully from N-able.')
        except Exception as e:
            spinner.fail(f'Failed to fetch data from N-able for {customer_name}. Error: {str(e)}')
            return

    with halo.Halo(text='Updating customer data...', spinner='dots') as spinner:
        # Open the customer_data.json file and read the data
        with open(customer_data_file, 'r') as f:
            customer_data = json.load(f)

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the n_able value with license count
                entry['applications']['n_able'] = total_count
                break

        # Write the updated customer data back to the file
        with open(customer_data_file, 'w') as f:
            json.dump(customer_data, f, indent=4)

        spinner.succeed(f'{customer_name} data updated successfully.')

    return total_count


def org3_n_able():
    customer_name = 'org3'
    customer_id = 629

    with halo.Halo(text='Fetching data from N-able...', spinner='dots') as spinner:
        try:
            total_count = retrieve_data_from_nable(customer_id)
            spinner.succeed(f'{customer_name} data fetched successfully from N-able.')
        except Exception as e:
            spinner.fail(f'Failed to fetch data from N-able for {customer_name}. Error: {str(e)}')
            return

    with halo.Halo(text='Updating customer data...', spinner='dots') as spinner:
        # Open the customer_data.json file and read the data
        with open(customer_data_file, 'r') as f:
            customer_data = json.load(f)

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the n_able value with license count
                entry['applications']['n_able'] = total_count
                break

        # Write the updated customer data back to the file
        with open(customer_data_file, 'w') as f:
            json.dump(customer_data, f, indent=4)

        spinner.succeed(f'{customer_name} data updated successfully.')

    return total_count

def org4_n_able():
    customer_name = 'org4'
    customer_id = 928

    with halo.Halo(text='Fetching data from N-able...', spinner='dots') as spinner:
        try:
            total_count = retrieve_data_from_nable(customer_id)
            spinner.succeed(f'{customer_name} data fetched successfully from N-able.')
        except Exception as e:
            spinner.fail(f'Failed to fetch data from N-able for {customer_name}. Error: {str(e)}')
            return

    with halo.Halo(text='Updating customer data...', spinner='dots') as spinner:
        # Open the customer_data.json file and read the data
        with open(customer_data_file, 'r') as f:
            customer_data = json.load(f)

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the n_able value with license count
                entry['applications']['n_able'] = total_count
                break

        # Write the updated customer data back to the file
        with open(customer_data_file, 'w') as f:
            json.dump(customer_data, f, indent=4)

        spinner.succeed(f'{customer_name} data updated successfully.')

    return total_count

def org5_n_able():
    customer_name = 'org5'
    customer_id = 922

    with halo.Halo(text='Fetching data from N-able...', spinner='dots') as spinner:
        try:
            total_count = retrieve_data_from_nable(customer_id)
            spinner.succeed(f'{customer_name} data fetched successfully from N-able.')
        except Exception as e:
            spinner.fail(f'Failed to fetch data from N-able for {customer_name}. Error: {str(e)}')
            return

    with halo.Halo(text='Updating customer data...', spinner='dots') as spinner:
        # Open the customer_data.json file and read the data
        with open(customer_data_file, 'r') as f:
            customer_data = json.load(f)

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the n_able value with license count
                entry['applications']['n_able'] = total_count
                break

        # Write the updated customer data back to the file
        with open(customer_data_file, 'w') as f:
            json.dump(customer_data, f, indent=4)

        spinner.succeed(f'{customer_name} data updated successfully.')

    return total_count


#call all functions
org1_n_able()
org2_n_able()
org3_n_able()
org4_n_able()
org5_n_able()

