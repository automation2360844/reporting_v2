'''
Purpose of this script is to pull data from the Cisco Umbrella API and save it to customer_data.json
'''

import requests
import json
import os
import time
import jmespath
import halo

customer_data_file = '../json/customer_data.json'

#Export/Set the environment variables
os.environ['org_TOKEN_URL'] = 'https://api.umbrella.com/auth/v2/token'
os.environ['org_API_KEY'] = ''
os.environ['org_API_SECRET'] = ''
client_id = os.environ.get('org_API_KEY')
client_secret = os.environ.get('org_API_SECRET')


class orgUmbrellaAPI():
	def __init__(self, token_url, client_id, client_secret):
		self.token_url = token_url
		self.client_id = client_id
		self.client_secret = client_secret

		try:
			self.access_token = self.getAccessToken()
			if self.access_token is None:
				raise Exception("Request for access token failed")
		except Exception as e:
			print(e)
	def getAccessToken(self):
		try:
			payload={}
			rsp = requests.post(self.token_url, data=payload, auth=(self.client_id, self.client_secret))
			rsp.raise_for_status()
		except Exception as e:
			print(e)
			return None
		else:
			clock_skew = 300
			self.access_token_expiration = int(time.time()) + rsp.json()['expires_in'] - clock_skew
			return rsp.json()['access_token']

	def __str__(self):
		return "token_url: {} client_id: {} client_secret: {} access_token_expiration: {}".format(self.token_url, self.client_id, self.client_secret, self.access_token_expiration)

def refreshToken(decorated):
	def wrapper(api, *args, **kwargs):
		if int(time.time()) > api.access_token_expiration:
			api.access_token = api.getAccessToken()
		return decorated(api, *args, **kwargs)
	return wrapper

@refreshToken
def callUmbrellaApi(api, path):
    api_headers = {}
    api_headers['Authorization'] = 'Bearer ' + api.access_token
    r = requests.get('https://api.umbrella.com/deployments/v2' + path, headers=api_headers)
    #print(json.dumps(r.json(), indent=4))
    return r

def org_umbrella():
    customer_name = 'org Internal'
    # Export/Set the environment variables
    token_url = os.environ.get('org_TOKEN_URL') or 'https://api.umbrella.com/auth/v2/token'
    client_id = os.environ.get('org_API_KEY')
    client_secret = os.environ.get('org_API_SECRET')

    api = orgUmbrellaAPI(token_url, client_id, client_secret)

    with halo.Halo(text='Fetching data from Cisco Umbrella...', spinner='dots') as spinner:
        try:
            umbrella_api_get = callUmbrellaApi(api, '/roamingcomputers')
            jmespath_filter = '[].name | length(@)'
            groups_and_devices = jmespath.search(jmespath_filter, umbrella_api_get.json())
            count = json.dumps(groups_and_devices, indent=4)
            total_count = int(count)
            spinner.succeed(f'{customer_name} data fetched successfully from Cisco Umbrella.')
        except Exception as e:
            spinner.fail(f'Failed to fetch data from Cisco Umbrella for {customer_name}. Error: {str(e)}')
            return

    with halo.Halo(text=f'Updating {customer_name} data...', spinner='dots') as spinner:
        # Open the customer_data.json file and read the data
        with open(customer_data_file, 'r') as f:
            customer_data = json.load(f)

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the umbrella value with license count
                entry['applications']['cisco_umbrella'] = total_count
                break

        # Write the updated customer data back to the file
        with open(customer_data_file, 'w') as f:
            json.dump(customer_data, f, indent=4)

        spinner.succeed(f'{customer_name} data updated successfully.')

    return total_count


org_umbrella()
