import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import datetime

username = ""
password = ""
mail_from = ""

current_month = datetime.datetime.now().month
month_names = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
month = month_names[current_month - 1]
current_year = datetime.datetime.now().year

mail_to = []

mail_subject = f"Managed Services | Monthly Device Count for {month} {current_year}"
mail_body = """Attached you will find the latest report with the device counts for Managed Services customers.

If you find that any data is missing or you have any questsions, please don't hesitate to reach out to your friendly automation nerds.
"""
mail_attachment="../csv/customer_data.csv"
mail_attachment_name="customer_data.csv"

mimemsg = MIMEMultipart()
mimemsg['From'] = mail_from
mimemsg['To'] = ", ".join(mail_to)
mimemsg['Subject'] = mail_subject
mimemsg.attach(MIMEText(mail_body, 'plain'))

with open(mail_attachment, "rb") as attachment:
    mimefile = MIMEBase('application', 'octet-stream')
    mimefile.set_payload((attachment).read())
    encoders.encode_base64(mimefile)
    mimefile.add_header('Content-Disposition', "attachment; filename= %s" % mail_attachment_name)
    mimemsg.attach(mimefile)
    connection = smtplib.SMTP(host='smtp.office365.com', port=587)
    connection.starttls()
    connection.login(username,password)
    connection.send_message(mimemsg)
    connection.quit()
