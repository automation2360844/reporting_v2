'''
The purpose of this script will be to read the device count we have gathered from API and saved in customer_list.json and place those in the
respective customer entries in the customer_data.json
'''

import json
import halo

file_path = '../json/customer_list.json'
file_data = '../json/customer_data.json'

try:
    # Open the customer_list.json file and read the data
    with open(file_path, 'r') as f:
        customer_list = json.load(f)

    # Open the customer_data.json file and read the data
    with open(file_data, 'r') as f:
        customer_data = json.load(f)

    # Initialize the Halo spinner
    spinner = halo.Halo(text='Updating customer_data', spinner='dots')

    # Loop through the customer_list and update customer_data with device count
    for customer in customer_list:
        customer_name = customer['name'].replace('\u200B', '')
        num_of_hosts = customer['numOfHosts']

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the logicmonitor value with numOfHosts count
                entry['applications']['logicmonitor'] = num_of_hosts
                break

        # Show successful iteration with Halo spinner
        spinner.succeed(f'Updated customer_data.json for {customer_name}')

    # Stop the Halo spinner
    spinner.stop()

    # Write the updated customer_data back to the file
    with open(file_data, 'w') as f:
        json.dump(customer_data, f, indent=4)
        
except Exception as e:
    spinner.fail(f'Failed to update customer_data.json. Error: {str(e)}')
