'''
This script pulls cradlepoint data and updates the customer_data.json file with the device count.
'''

import json
import requests
import jmespath
import halo


customer_data_file = '../json/customer_data.json'

def org1_cradlepoint():
    customer_name = 'org1'
    # https://developer.cradlepoint.com/ It has code examples for Python
    url = 'https://www.cradlepointecm.com/api/v2/routers/?limit=200&offset=20'
    #url = 'https://www.cradlepointecm.com/api/v2/net_devices/?limit=500&offset=209'

    # To retrieve the CP-API-ID and the CP-API-Key
    # go to Tools > NetCloudAPI > API Portal
    headers = {
        'X-CP-API-ID': '',
        'X-CP-API-KEY': '',
        'X-ECM-API-ID': '',
        'X-ECM-API-KEY': '',
        'Content-Type': 'application/json' 
    }

    jmespath_filter = 'data[].account | length(@)'

    with halo.Halo(text='Fetching Cradlepoint device count...', spinner='dots') as spinner:
        try:
            cradlepoint_get = requests.get(url, headers=headers)
            devices_count = jmespath.search(jmespath_filter, cradlepoint_get.json())
            count = json.dumps(devices_count, indent=4)
            total_count = int(count)
            spinner.succeed(f'{customer_name} Cradlepoint device count fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Cradlepoint device count for {customer_name}. Error: {str(e)}')
            return

    with halo.Halo(text='Updating customer data...', spinner='dots') as spinner:
        # Open the customer_data.json file and read the data
        with open(customer_data_file, 'r') as f:
            customer_data = json.load(f)

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the cradlepoint value with license count
                entry['applications']['cradlepoint'] = total_count
                break

        # Write the updated customer data back to the file
        with open(customer_data_file, 'w') as f:
            json.dump(customer_data, f, indent=4)

        spinner.succeed(f'{customer_name} data updated successfully.')

    return total_count



org1_cradlepoint()
