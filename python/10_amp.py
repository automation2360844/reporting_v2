'''
Purpose of this script is to get a total of AMP licenses and save it to customer_data.json
'''

import requests
import jmespath
import json
import halo

customer_data_file = '../json/customer_data.json'


# API v1
def org_amp():
    customer_name = 'org Internal'
    # Set Client Variables
    amp_client_id = ''
    amp_api_key = ''
    url = 'https://api.amp.cisco.com/v1/computers'

    # JMESpath parse
    amp_jmespath = 'metadata.results.total'

    with halo.Halo(text='Fetching data from Cisco AMP...', spinner='dots') as spinner:
        try:
            # JMESpath parse
            amp_get_stuff = requests.get(url, auth=(amp_client_id, amp_api_key))
            groups_and_devices = jmespath.search(amp_jmespath, amp_get_stuff.json())
            count = json.dumps(groups_and_devices, indent=4)
            total_count = int(count)
            spinner.succeed(f'{customer_name} data fetched successfully from Cisco AMP.')
        except Exception as e:
            spinner.fail(f'Failed to fetch data from Cisco AMP for {customer_name}. Error: {str(e)}')
            return

    with halo.Halo(text='Updating customer data...', spinner='dots') as spinner:
        # Open the customer_data.json file and read the data
        with open(customer_data_file, 'r') as f:
            customer_data = json.load(f)

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the cisco_amp value with license count
                entry['applications']['cisco_amp'] = total_count
                break

        # Write the updated customer data back to the file
        with open(customer_data_file, 'w') as f:
            json.dump(customer_data, f, indent=4)

        spinner.succeed(f'{customer_name} data updated successfully.')

    return total_count


org_amp()
