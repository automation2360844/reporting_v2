'''
Purpose of this script would be to pull inventory count from the Meraki API and store that within the customer_data.json file under the customer name under the meraki key
'''

import meraki
from sys import implementation
from time import time
from pyparsing import empty
from ssl import CertificateError
import json
import halo

customer_data_file = '../json/customer_data.json'

def org_meraki():
    customer_name = 'org Internal'
    # Global variables
    api_key = ''
    organization_id = ''
    dashboard = meraki.DashboardAPI(api_key, output_log=False, print_console=False)

    with halo.Halo(text=f'Fetching Meraki device count for {customer_name}...', spinner='dots') as spinner:
        try:
            # Request
            response = dashboard.organizations.getOrganizationInventoryDevices(organization_id, total_pages='all')
            total_count = len(response)

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the meraki value with device count
                    entry['applications']['meraki'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'Meraki device count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Meraki device count for {customer_name}. Error: {str(e)}')
            return

    return total_count

def org_meraki_mdr():
    customer_name = 'org Internal'
    # Global variables
    api_key = ''
    dashboard = meraki.DashboardAPI(api_key, output_log=False, print_console=False)
    network_id = ''

    with halo.Halo(text=f'Fetching Meraki MDR device count for {customer_name}...', spinner='dots') as spinner:
        try:
            # Request
            response = dashboard.sm.getNetworkSmDevices(network_id, total_pages='all')
            total_count = len(response)

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the meraki_mdr value with device count
                    entry['applications']['meraki_mdr'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'Meraki MDR device count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Meraki MDR device count for {customer_name}. Error: {str(e)}')
            return

    return total_count

def org2_meraki():
    customer_name = 'org2'
    # Global variables
    api_key = ''
    organization_id = ''
    dashboard = meraki.DashboardAPI(api_key, output_log=False, print_console=False)

    with halo.Halo(text=f'Fetching Meraki device count for {customer_name}...', spinner='dots') as spinner:
        try:
            # Request
            response = dashboard.organizations.getOrganizationInventoryDevices(organization_id, total_pages='all')
            total_count = len(response)

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the meraki value with device count
                    entry['applications']['meraki'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'Meraki device count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Meraki device count for {customer_name}. Error: {str(e)}')
            return

    return total_count

def org3_meraki():
    customer_name = 'org3'
    # Global variables
    api_key = ''
    organization_id = ''
    dashboard = meraki.DashboardAPI(api_key, output_log=False, print_console=False)

    with halo.Halo(text=f'Fetching Meraki device count for {customer_name}...', spinner='dots') as spinner:
        try:
            # Request
            response = dashboard.organizations.getOrganizationInventoryDevices(organization_id, total_pages='all')
            total_count = len(response)

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the meraki value with device count
                    entry['applications']['meraki'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'Meraki device count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Meraki device count for {customer_name}. Error: {str(e)}')
            return

    return total_count


def org4_meraki():
    customer_name = 'org4'
    # Global variables
    api_key = ''
    organization_id = ''
    dashboard = meraki.DashboardAPI(api_key, output_log=False, print_console=False)

    with halo.Halo(text=f'Fetching Meraki device count for {customer_name}...', spinner='dots') as spinner:
        try:
            # Request
            response = dashboard.organizations.getOrganizationInventoryDevices(organization_id, total_pages='all')
            total_count = len(response)

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the meraki value with device count
                    entry['applications']['meraki'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'Meraki device count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Meraki device count for {customer_name}. Error: {str(e)}')
            return

    return total_count

def org5_meraki():
    customer_name = 'org5'
    # Global variables
    api_key = ''
    organization_id_bgp = ''
    dashboard = meraki.DashboardAPI(api_key, output_log=False, print_console=False)

    with halo.Halo(text=f'Fetching Meraki device count for {customer_name}...', spinner='dots') as spinner:
        try:
            # Request inventory list from org5 BGP Org
            response_bgp = dashboard.organizations.getOrganizationInventoryDevices(organization_id_bgp, total_pages='all')
            bgp_count = len(response_bgp)

            total_count = int(bgp_count)

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the meraki value with device count
                    entry['applications']['meraki'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'Meraki device count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Meraki device count for {customer_name}. Error: {str(e)}')
            return

    return total_count


def org6_meraki():
    customer_name = 'org6'
    # Global variables
    api_key = ''
    organization_id = ''
    dashboard = meraki.DashboardAPI(api_key, output_log=False, print_console=False)

    with halo.Halo(text=f'Fetching Meraki device count for {customer_name}...', spinner='dots') as spinner:
        try:
            # Request inventory list
            response = dashboard.organizations.getOrganizationInventoryDevices(organization_id, total_pages='all')
            total_count = len(response)

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the meraki value with device count
                    entry['applications']['meraki'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'Meraki device count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Meraki device count for {customer_name}. Error: {str(e)}')
            return

    return total_count


#Use this for troubleshooting in the event an API doesn't respond
# print('org Meraki: ' + str(org_meraki()))
# print('org Meraki MDR: ' + str(org_meraki_mdr()))
# print('org2 Meraki: ' + str(org2_meraki()))
# print('org3 Meraki: ' + str(org3_meraki()))
# print('org4 Meraki: ' + str(org4_meraki()))
# print('org5 Meraki: ' + str(org5_meraki()))
# print('org6 Meraki: ' + str(org6_meraki()))

org_meraki()
org_meraki_mdr()
org2_meraki()
org3_meraki()
org4_meraki()
org5_meraki()
org6_meraki()
