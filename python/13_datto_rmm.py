'''
Purpose of this script is to run a powershell script in the powershell script folder that will request a new token from Datto RMM and save it to a text file.
Then make an API call based on the token within the token.txt file and save the data to customer_data.json
'''

import json
import requests
import jmespath
import subprocess
from pathlib import Path
import os
import halo


customer_data_file = '../json/customer_data.json'

# cmd = 'pwsh ~/GitLab/managed_services_automation/reporting_v2/powershell/datto_rmm_token_request.ps1'
# os.system(cmd)

subprocess.run(['pwsh', '../powershell/datto_rmm_token_request.ps1'])

path = '../powershell/token.txt'

#This reads the text file that was created by powershell and assignes a value 
with open(path) as f:
    token = f.read()

access_token = token.replace("\n", "")

def datto_org1():
    customer_name = 'org1'
    # This runs a PowerShell script that creates a text file with the new token code.

    # API variables for request
    apiUrl = 'https://zinfandel-api.centrastage.net/api/v2/site/'

    # Datto RMM org numbers
    site_id_health_efficient_devices = '8e8a38a1-ae2d-4ed9-8aa0-ec43a14d9474'

    jmespath_filter = 'devicesStatus.numberOfDevices'

    # Build session
    datto_session = requests.session()

    # Update header
    datto_session.headers.update({'Authorization': 'Bearer ' + access_token})

    with halo.Halo(text='Fetching data from Datto RMM...', spinner='dots') as spinner:
        try:
            datto_get_stuff = datto_session.get(apiUrl + site_id_health_efficient_devices).json()
            devices = jmespath.search(jmespath_filter, datto_get_stuff)
            count = json.dumps(devices, indent=4)
            total_count = int(count)
            spinner.succeed(f'{customer_name} data fetched successfully from Datto RMM.')
        except Exception as e:
            spinner.fail('Failed to fetch data from Datto RMM. Error: {}'.format(str(e)))
            return

    with halo.Halo(text=f'Updating {customer_name} data...', spinner='dots') as spinner:
        # Open the customer_data.json file and read the data
        with open(customer_data_file, 'r') as f:
            customer_data = json.load(f)

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the Datto RMM value with license count
                entry['applications']['datto_rmm'] = total_count
                break

         # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the Datto RMM value with license count
                entry['applications']['datto_fileprotection'] = total_count
                break

        # Write the updated customer data back to the file
        with open(customer_data_file, 'w') as f:
            json.dump(customer_data, f, indent=4)

        spinner.succeed(f'{customer_name} data updated successfully.')

    return total_count

datto_org1()
