import requests
import json
import hashlib
import base64
import time
import hmac

customer_list = '../json/customer_list.json'
output_file = '../json/customer_list_ext.json'
customer_data_file = '../json/customer_data.json'

with open(customer_list) as json_file:
    data = json.load(json_file)

AccessId = ''
AccessKey = ''
Company = ''

httpVerb = 'GET'

all_responses = {
    "total_num_of_hosts": 0,
    "total_num_of_cloud_devices": 0
}

# Loop through each customerId
for entry in data:
    customerId = entry['id']
    customerName = entry['name']

    resourcePath = f'/device/groups/{customerId}/'
    data = ''

    url = 'https://' + Company + '.logicmonitor.com/santaba/rest' + resourcePath
    epoch = str(int(time.time() * 1000))
    requestVars = httpVerb + epoch + data + resourcePath
    hmac1 = hmac.new(AccessKey.encode(), msg=requestVars.encode(), digestmod=hashlib.sha256).hexdigest()
    signature = base64.b64encode(hmac1.encode())
    auth = 'LMv1 ' + AccessId + ':' + signature.decode() + ':' + epoch
    headers = {'Content-Type': 'application/json', 'Authorization': auth}

    response = requests.get(url, data=data, headers=headers).json()
    num_of_hosts = response['data']['numOfHosts']
    num_of_aws_devices = response['data']['numOfAWSDevices']
    num_of_azure_devices = response['data']['numOfAzureDevices']
    num_of_gcp_devices = response['data']['numOfGcpDevices']

    total_num_of_hosts = num_of_hosts - (num_of_aws_devices + num_of_azure_devices + num_of_gcp_devices)
    total_num_of_cloud_devices = num_of_aws_devices + num_of_azure_devices + num_of_gcp_devices

    all_responses[customerName] = {
        "total_num_of_hosts": total_num_of_hosts,
        "total_num_of_cloud_devices": total_num_of_cloud_devices
    }

    all_responses["total_num_of_hosts"] += total_num_of_hosts
    all_responses["total_num_of_cloud_devices"] += total_num_of_cloud_devices

# Save all responses to a single JSON file
with open(output_file, 'w') as outfile:
    outfile.write(json.dumps(all_responses, indent=4))

# Update customer.data.json with logicmonitor and logicmonitor_cloud values
with open(customer_data_file) as json_file:
    customer_data = json.load(json_file)

for customer_entry in customer_data:
    customer_name = customer_entry['customer']
    if customer_name in all_responses:
        total_num_of_hosts = all_responses[customer_name]['total_num_of_hosts']
        total_num_of_cloud_devices = all_responses[customer_name]['total_num_of_cloud_devices']
        customer_entry['applications']['logicmonitor'] = total_num_of_hosts
        customer_entry['applications']['logicmonitor_cloud'] = total_num_of_cloud_devices

with open(customer_data_file, 'w') as json_file:
    json.dump(customer_data, json_file, indent=4)
