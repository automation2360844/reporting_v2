'''
Purpose of this script is to pull the total number of O365 licenses from customer tenants and save them to customer_data.json under the "o365" key.
'''
import msal
import requests
import json
import jmespath
import halo

customer_data_file = '../json/customer_data.json'

def get_access_token(client_id, client_secret, authority, scope):
    client = msal.ConfidentialClientApplication(client_id, authority=authority, client_credential=client_secret)
    token_result = client.acquire_token_silent(scope, account=None)

    if token_result:
        access_token = 'Bearer ' + token_result['access_token']
    else:
        token_result = client.acquire_token_for_client(scopes=scope)
        access_token = 'Bearer ' + token_result['access_token']

    return access_token

def get_license_count(access_token, url, license_count):
    headers = {'Authorization': access_token, 'ConsistencyLevel': 'eventual'}
    result = requests.get(url=url, headers=headers)
    result_json = result.json()
    used_count = jmespath.search(license_count, result_json)
    count = int(json.dumps(used_count, indent=4))
    return count

def org_o365():
    customer_name = "org Internal"
    client_id = ''
    client_secret = ''
    authority = 'https://login.microsoftonline.com/7806d629-5cc0-4dcd-b7d4-a34474b31ed6'
    scope = ['https://graph.microsoft.com/.default']

    access_token = get_access_token(client_id, client_secret, authority, scope)

    o365_e1 = '7806d629-5cc0-4dcd-b7d4-a34474b31ed6_18181a46-0d4e-45cd-891e-60aabd171b4e'
    o365_e3 = '7806d629-5cc0-4dcd-b7d4-a34474b31ed6_6fd2c87f-b296-42f0-b197-1e91e994b900'
    o365_f3 = '7806d629-5cc0-4dcd-b7d4-a34474b31ed6_4b585984-651b-448a-9e53-3b10f069cf7f'

    license_count = "consumedUnits"

    o365_e1_url = "https://graph.microsoft.com/v1.0/subscribedSkus/" + o365_e1
    o365_e3_url = "https://graph.microsoft.com/v1.0/subscribedSkus/" + o365_e3
    o365_f3_url = "https://graph.microsoft.com/v1.0/subscribedSkus/" + o365_f3

    with halo.Halo(text=f'Fetching O365 license count for {customer_name}...', spinner='dots') as spinner:
        try:
            o365_e1_count = get_license_count(access_token, o365_e1_url, license_count)
            o365_e3_count = get_license_count(access_token, o365_e3_url, license_count)
            o365_f3_count = get_license_count(access_token, o365_f3_url, license_count)

            total_count = int(o365_e1_count + o365_e3_count + o365_f3_count)

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the o365 value with license count
                    entry['applications']['o365'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'O365 license count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch O365 license count for {customer_name}. Error: {str(e)}')
            return

    return total_count


def org2_o365():
    customer_name = 'org2 Group'
    # Enter the details of your AAD app registration
    client_id = ''
    client_secret = ''
    authority = 'https://login.microsoftonline.com/39e75956-446f-44d9-b8cd-2535b9a4b8fb'
    scope = ['https://graph.microsoft.com/.default']

    access_token = get_access_token(client_id, client_secret, authority, scope)

    o365_e1 = '39e75956-446f-44d9-b8cd-2535b9a4b8fb_18181a46-0d4e-45cd-891e-60aabd171b4e'

    license_count = "consumedUnits"

    o365_e1_url = "https://graph.microsoft.com/v1.0/subscribedSkus/" + o365_e1

    with halo.Halo(text=f'Fetching O365 license count for {customer_name}...', spinner='dots') as spinner:
        try:
            o365_e1_count = get_license_count(access_token, o365_e1_url, license_count)
            o365_e1_count_int = int(json.dumps(o365_e1_count, indent=4))
            total_count = o365_e1_count_int

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the o365 value with license count
                    entry['applications']['o365'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'O365 license count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch O365 license count for {customer_name}. Error: {str(e)}')
            return

    return total_count

def org3_o365():
    customer_name = 'org3 Health & Wellness'
    # Enter the details of your AAD app registration
    client_id = ''
    client_secret = ''
    authority = 'https://login.microsoftonline.com/033664d6-d35c-4cc6-b746-c39f42e8ca1f'
    scope = ['https://graph.microsoft.com/.default']

    access_token = get_access_token(client_id, client_secret, authority, scope)

    # License IDs
    o365_e1 = '033664d6-d35c-4cc6-b746-c39f42e8ca1f_18181a46-0d4e-45cd-891e-60aabd171b4e'
    o365_e3 = '033664d6-d35c-4cc6-b746-c39f42e8ca1f_6fd2c87f-b296-42f0-b197-1e91e994b900'

    # jmesparse filter
    license_count = "consumedUnits"

    # Copy access_token and specify the MS Graph API endpoint you want to call, e.g. 'https://graph.microsoft.com/v1.0/groups' to get all groups in your organization
    # URL variables for each license ID
    o365_e1_url = "https://graph.microsoft.com/v1.0/subscribedSkus/" + o365_e1
    o365_e3_url = "https://graph.microsoft.com/v1.0/subscribedSkus/" + o365_e3

    with halo.Halo(text=f'Fetching O365 license count for {customer_name}...', spinner='dots') as spinner:
        try:
            o365_e1_count = get_license_count(access_token, o365_e1_url, license_count)
            o365_e3_count = get_license_count(access_token, o365_e3_url, license_count)

            o365_e1_count = int(json.dumps(o365_e1_count, indent=4))
            o365_e3_count = int(json.dumps(o365_e3_count, indent=4))
            total_count = int(o365_e1_count + o365_e3_count)

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the o365 value with license count
                    entry['applications']['o365'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'O365 license count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch O365 license count for {customer_name}. Error: {str(e)}')
            return

    return total_count


def org4_o365():
    customer_name = 'org4'
    # Enter the details of your AAD app registration
    client_id = ''
    client_secret = ''
    authority = 'https://login.microsoftonline.com/fed79e58-4c45-4623-a9af-baec81ed5495'
    scope = ['https://graph.microsoft.com/.default']

    access_token = get_access_token(client_id, client_secret, authority, scope)

    # License IDs
    o365_buss_ess = 'fed79e58-4c45-4623-a9af-baec81ed5495_3b555118-da6a-4418-894f-7df1e2096870'
    o365_buss_pre = 'fed79e58-4c45-4623-a9af-baec81ed5495_cbdc14ab-d96c-4c30-b9f4-6ada7cdc1d46'

    # jmesparse filter
    license_count = "consumedUnits"

    # Copy access_token and specify the MS Graph API endpoint you want to call, e.g. 'https://graph.microsoft.com/v1.0/groups' to get all groups in your organization
    # URL variables for each license ID
    o365_buss_ess_url = "https://graph.microsoft.com/v1.0/subscribedSkus/" + o365_buss_ess
    o365_buss_pre_url = "https://graph.microsoft.com/v1.0/subscribedSkus/" + o365_buss_pre

    with halo.Halo(text=f'Fetching O365 license count for {customer_name}...', spinner='dots') as spinner:
        try:
            o365_buss_ess_count = get_license_count(access_token, o365_buss_ess_url, license_count)
            o365_buss_pre_count = get_license_count(access_token, o365_buss_pre_url, license_count)

            o365_buss_ess_count = int(json.dumps(o365_buss_ess_count, indent=4))
            o365_buss_pre_count = int(json.dumps(o365_buss_pre_count, indent=4))
            total_count = int(o365_buss_ess_count + o365_buss_pre_count)

            # Open the customer_data.json file and read the data
            with open(customer_data_file, 'r') as f:
                customer_data = json.load(f)

            # Find the corresponding customer entry in customer_data
            for entry in customer_data:
                if entry['customer'] == customer_name:
                    # Update the o365 value with license count
                    entry['applications']['o365'] = total_count
                    break

            with open(customer_data_file, 'w') as f:
                json.dump(customer_data, f, indent=4)

            spinner.succeed(f'O365 license count for {customer_name} fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch O365 license count for {customer_name}. Error: {str(e)}')
            return

    return total_count


# print('org :' + str(org_o365()))
# print('org2 :' + str(org2_o365()))
# print('org3 :' + str(org3_o365()))

org_o365()
org2_o365()
org4_o365()
org3_o365()
