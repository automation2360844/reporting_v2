'''
Purpose of this script will be to create a base JSON file for each customer. Each application will be a key value for that customer with the count of those devices
This will read the list from customer_list and will then create a new JSON file where each entry is a customer and the value key pairs are the applications and the count of those applications
'''
import json
from customer_list import customer_list
import halo

customer_data = []

# Add a customer that doesn't have a LogicMonitor presence.
customer_name = ['org1', 'org2', 'org3']
for customer in customer_name:
    customer_data.append({
        'customer': customer,
        'applications': {
            'logicmonitor': 0,
            'logicmonitor_cloud': 0,
            'o365': 0,
            'duo': 0,
            'meraki': 0,
            'meraki_mdr': 0,
            'proofpoint': 0,
            'automox': 0,
            'cradlepoint': 0,
            'n_able': 0,
            'cisco_amp': 0,
            'cisco_umbrella': 0,
            'datto_rmm': 0,
            'datto_fileprotection': 0,
        }
    })

def process_customer_list():
    # Create a Halo spinner object
    spinner = halo.Halo(text='Processing', spinner='dots')

    # Iterate over the customer list
    for index, customer in enumerate(customer_list()):
        customer_name = customer[0].replace('\u200B', '')
        customer_data.append({
            'customer': customer_name,
            'applications': {
                'logicmonitor': 0,
                'logicmonitor_cloud': 0,
                'o365': 0,
                'duo': 0,
                'meraki': 0,
                'meraki_mdr': 0,
                'proofpoint': 0,
                'automox': 0,
                'cradlepoint': 0,
                'n_able': 0,
                'cisco_amp': 0,
                'cisco_umbrella': 0,
                'datto_rmm': 0,
                'datto_fileprotection': 0,
            }
        })

        # Update the Halo spinner with the current progress
        spinner.text = f'Processing {customer} {index + 1}/{len(customer_list())}'
        spinner.start()  # Start the spinner

        # Simulate successful step
        spinner.succeed(f'Processed {customer[0]} {index + 1}/{len(customer_list())}')

    # Stop the Halo spinner
    spinner.stop_and_persist(symbol='✔', text='Processing complete!')


# Call the function to process the customer list
process_customer_list()

output_file = '../json/customer_data.json'

with open(output_file, 'w') as outfile:
    outfile.write(json.dumps(customer_data, indent=4))
    outfile.close()
