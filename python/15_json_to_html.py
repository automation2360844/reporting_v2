import os
import json
import pandas as pd
from halo import Halo

# Define the directory where the JSON files are located
docs_directory = os.path.abspath('../docs')
csv_directory = os.path.abspath('../csv')
csv_archives = os.path.abspath('../csv/archives')

# Define the path to your CSS stylesheet
css_path = os.path.abspath('../static/css/style.css')

# JSON file path
file_path = os.path.abspath('../json/customer_data.json')

# Create the docs and csv directories if they don't exist
os.makedirs(docs_directory, exist_ok=True)
os.makedirs(csv_directory, exist_ok=True)

# Create a spinner
spinner = Halo(text='Processing', spinner='dots')

try:
    # Start the spinner
    spinner.start()

    with open(file_path) as json_file:
        data = json.load(json_file)

    # Extract the customer names
    customer_names = [entry['customer'] for entry in data]

    # Create a DataFrame for storing the data
    df = pd.DataFrame(data)

    # Extract the application names
    application_names = list(df['applications'].iloc[0].keys())

    # Reshape the DataFrame
    df = pd.concat([df['customer'], pd.DataFrame(df['applications'].tolist())], axis=1)

    # Transpose the DataFrame
    df = df.set_index('customer').T.reset_index()

    # Rename the index column to 'Application'
    df = df.rename(columns={'index': 'Application'})

    # Create a new file with the same name but with .html extension in the docs directory
    html_file_path = os.path.join(docs_directory, 'customer_report.html')
    with open(html_file_path, 'w') as html_file:
        # Write the HTML file header
        html_header = f'''
        <!DOCTYPE html>
        <html>
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Managed Services Customer Report</title>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700">
            <link rel="stylesheet" href="{css_path}">
        </head>
        <body>
        '''

        html_file.write(html_header)

        # Write the HTML table
        html_file.write('<table>\n')

        # Write the header row
        html_file.write('<tr>\n')
        html_file.write('<th>Application</th>\n')
        for name in customer_names:
            html_file.write(f'<th>{name}</th>\n')
        html_file.write('</tr>\n')

        # Write the data rows
        for entry in df.iterrows():
            html_file.write('<tr>\n')
            html_file.write(f'<td>{entry[1]["Application"]}</td>\n')
            for name in customer_names:
                html_file.write(f'<td>{entry[1][name]}</td>\n')
            html_file.write('</tr>\n')

        # Write the HTML table footer
        html_file.write('</table>\n')

        # Write the HTML file footer
        html_file.write('</body>\n')
        html_file.write('</html>\n')

    # Create a new Markdown file with the name index.md in the docs directory
    md_file_path = os.path.join(docs_directory, 'index.md')
    with open(md_file_path, 'w') as md_file:
        # Write the Markdown table
        md_file.write('| Application |')
        for name in customer_names:
            md_file.write(f' {name} |')
        md_file.write('\n')
        md_file.write('| --- ')
        for _ in customer_names:
            md_file.write('| --- ')
        md_file.write('|\n')

        # Write the data rows
        for entry in df.iterrows():
            md_file.write(f'| {entry[1]["Application"]} ')
            for name in customer_names:
                md_file.write(f'| {entry[1][name]} ')
            md_file.write('|\n')

    # Create a new CSV file with the name customer_data.csv in the csv directory
    csv_file_path = os.path.join(csv_directory, 'customer_data.csv')
    df.to_csv(csv_file_path, index=False)

    # Create a new CSV file with the name customer_data.csv in the csv directory and have the year, month, and day
    csv_file_path = os.path.join(csv_archives, f'customer_data_{pd.Timestamp.now().strftime("%Y_%m_%d")}.csv')
    df.to_csv(csv_file_path, index=False)


    # Stop the spinner and show success
    spinner.succeed('JSON information has been converted to HTML, Markdown, and CSV!')

except Exception as e:
    # Stop the spinner and show failure
    spinner.fail(f'Error: {str(e)}')
