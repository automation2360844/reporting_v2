'''
Purpose of this script is to get the device count for each org in Automox and save it to the customer_data.json file.
'''

import requests
import jmespath
import json
import halo

customer_data_file = '../json/customer_data.json'

def get_automox_device_count(org_name):
    # Build session
    automox_session = requests.Session()
    token = ''
    
    # Update header
    automox_session.headers.update({'Authorization': 'Bearer ' + token})

    # JMESpath parse
    automox_jmespath_output = "[? name!=null]|[? contains(name,'{}')].device_count | [0]".format(org_name)

    automox_get_stuff = automox_session.get('https://console.automox.com/api/orgs')
    groups_and_devices = jmespath.search(automox_jmespath_output, automox_get_stuff.json())
    count = json.dumps(groups_and_devices, indent=4)
    total_count = int(count)
    
    return total_count

def org_automox():
    customer_name = 'org Internal'

    with halo.Halo(text='Fetching Automox device count...', spinner='dots') as spinner:
        try:
            total_count = get_automox_device_count('org')
            spinner.succeed(f'{customer_name} Automox device count fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Automox device count for {customer_name}. Error: {str(e)}')
            return

    with halo.Halo(text='Updating customer data...', spinner='dots') as spinner:
        # Open the customer_data.json file and read the data
        with open(customer_data_file, 'r') as f:
            customer_data = json.load(f)

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the automox value with license count
                entry['applications']['automox'] = total_count
                break

        # Write the updated customer data back to the file
        with open(customer_data_file, 'w') as f:
            json.dump(customer_data, f, indent=4)

        spinner.succeed(f'{customer_name} data updated successfully.')

    return total_count

def org2_automox():
    customer_name = 'org2 Group'

    with halo.Halo(text='Fetching Automox device count...', spinner='dots') as spinner:
        try:
            total_count = get_automox_device_count('org2')
            spinner.succeed(f'{customer_name} Automox device count fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Automox device count for {customer_name}. Error: {str(e)}')
            return

    with halo.Halo(text='Updating customer data...', spinner='dots') as spinner:
        # Open the customer_data.json file and read the data
        with open(customer_data_file, 'r') as f:
            customer_data = json.load(f)

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the automox value with license count
                entry['applications']['automox'] = total_count
                break

        # Write the updated customer data back to the file
        with open(customer_data_file, 'w') as f:
            json.dump(customer_data, f, indent=4)

        spinner.succeed(f'{customer_name} data updated successfully.')

    return total_count

def org3_automox():
    customer_name = 'org3'

    with halo.Halo(text='Fetching Automox device count...', spinner='dots') as spinner:
        try:
            total_count = get_automox_device_count('org3')
            spinner.succeed(f'{customer_name} Automox device count fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Automox device count for {customer_name}. Error: {str(e)}')
            return

    with halo.Halo(text='Updating customer data...', spinner='dots') as spinner:
        # Open the customer_data.json file and read the data
        with open(customer_data_file, 'r') as f:
            customer_data = json.load(f)

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the automox value with license count
                entry['applications']['automox'] = total_count
                break

        # Write the updated customer data back to the file
        with open(customer_data_file, 'w') as f:
            json.dump(customer_data, f, indent=4)

        spinner.succeed(f'{customer_name} data updated successfully.')

    return total_count

def org4_automox():
    customer_name = 'org4'

    with halo.Halo(text='Fetching Automox device count...', spinner='dots') as spinner:
        try:
            total_count = get_automox_device_count('org4')
            spinner.succeed(f'{customer_name} Automox device count fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Automox device count for {customer_name}. Error: {str(e)}')
            return

    with halo.Halo(text='Updating customer data...', spinner='dots') as spinner:
        # Open the customer_data.json file and read the data
        with open(customer_data_file, 'r') as f:
            customer_data = json.load(f)

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the automox value with license count
                entry['applications']['automox'] = total_count
                break

        # Write the updated customer data back to the file
        with open(customer_data_file, 'w') as f:
            json.dump(customer_data, f, indent=4)

        spinner.succeed(f'{customer_name} data updated successfully.')

    return total_count

def org5_automox():
    customer_name = 'org5'

    with halo.Halo(text='Fetching Automox device count...', spinner='dots') as spinner:
        try:
            total_count = get_automox_device_count('org5')
            spinner.succeed(f'{customer_name} Automox device count fetched successfully.')
        except Exception as e:
            spinner.fail(f'Failed to fetch Automox device count for {customer_name}. Error: {str(e)}')
            return

    with halo.Halo(text='Updating customer data...', spinner='dots') as spinner:
        # Open the customer_data.json file and read the data
        with open(customer_data_file, 'r') as f:
            customer_data = json.load(f)

        # Find the corresponding customer entry in customer_data
        for entry in customer_data:
            if entry['customer'] == customer_name:
                # Update the automox value with license count
                entry['applications']['automox'] = total_count
                break

        # Write the updated customer data back to the file
        with open(customer_data_file, 'w') as f:
            json.dump(customer_data, f, indent=4)

        spinner.succeed(f'{customer_name} data updated successfully.')

    return total_count


#call functions
org_automox()
org2_automox()
org3_automox()
org4_automox()
org5_automox()
